//
//  ViewController.swift
//  ImageTransfer
//
//  Created by Tzu-Yi Lin on 2014/8/4.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

import UIKit
import AssetsLibrary
import ImageIO

class ViewController: UICollectionViewController {

    let library = ALAssetsLibrary()
    let dispatchQueue = dispatch_queue_create("saveToCameraRoll", DISPATCH_QUEUE_SERIAL)
    
    class ImageData {
        var fileurl: NSURL? {
            didSet {
                self.thumbnailImage = nil
                createThumbnail()
            }
        }
        
        var complete = false
        
        init(url: NSURL?) {
            fileurl = url
        }
        
        private func createThumbnail() {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                // create file by image/IO
                let option: [NSObject : AnyObject] = [
                    kCGImageSourceCreateThumbnailFromImageIfAbsent: kCFBooleanTrue,
                    kCGImageSourceCreateThumbnailWithTransform: kCFBooleanTrue,
                    kCGImageSourceShouldAllowFloat: kCFBooleanTrue,
                    kCGImageSourceThumbnailMaxPixelSize: 150
                ]

                let imageSource = CGImageSourceCreateWithURL(self.fileurl, option)
                let cgImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, option)
                self.thumbnailImage = UIImage(CGImage: cgImage)
                
                if let cell = self.targetCell {
                    dispatch_async(dispatch_get_main_queue()) {
                        cell.imageView.image = self.thumbnailImage
                    }
                }
            }
        }
        
        weak var targetCell: ImageCollectionViewCell? {
            didSet {
                if targetCell != nil {
                    createThumbnail()
                }
            }
        }

        var thumbnailImage: UIImage?
        var fullSizeImage: UIImage {
            return UIImage(contentsOfFile: fileurl!.path!)!
        }
    } // end ImageData


    var imageArray: [ImageData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadLibrary()
        collectionView?.alwaysBounceVertical = true
    }

    private func loadLibrary() {
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as! String
        
        let fileManager = NSFileManager.defaultManager()
        let contents = fileManager.contentsOfDirectoryAtURL(
            NSURL.fileURLWithPath(path)!,
            includingPropertiesForKeys: [NSURLPathKey],
            options: .SkipsSubdirectoryDescendants | .SkipsHiddenFiles | .SkipsPackageDescendants,
            error: nil
        ) as! [NSURL]
        
        NSLog("number of file: \(contents.count)")
        for url in contents {
            var imageData = ImageData(url: url)
            imageArray.append(imageData)
        }
    }
    
    @IBAction func importImages(sender: AnyObject?) {
        
        self.navigationItem.leftBarButtonItem?.enabled = false
        self.navigationItem.rightBarButtonItem?.enabled = false

        var count = 0

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0)) {

            var semaphore = dispatch_semaphore_create(0)

            for (index, imageItem) in enumerate(self.imageArray) {
                dispatch_async(self.dispatchQueue) {

                    let image = imageItem.fullSizeImage

                    self.library.writeImageToSavedPhotosAlbum(image.CGImage, orientation: ALAssetOrientation(rawValue: image.imageOrientation.rawValue)!) { assertURL, error in

                        if error == nil {
                            imageItem.complete = true
                            dispatch_async(dispatch_get_main_queue()) {
                                let indexPaths = [NSIndexPath(forItem: index, inSection: 0)]
                                self.collectionView?.reloadItemsAtIndexPaths(indexPaths)
                            }
                        }

                        if ++count == self.imageArray.count {
                            dispatch_async(dispatch_get_main_queue()) {
                                self.navigationItem.leftBarButtonItem?.enabled = true
                                self.navigationItem.rightBarButtonItem?.enabled = true
                            }
                        }

                        dispatch_semaphore_signal(semaphore)
                    }
                }
                
                dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
            }

        }
    }
    
    @IBAction func removeComplete(sender: AnyObject?) {
        
        let fileManager = NSFileManager.defaultManager()
        var indexPaths = [NSIndexPath]()
        
        for (index, imageItem) in enumerate(imageArray) {
            if imageItem.complete {
                indexPaths.append(NSIndexPath(forItem: index, inSection: 0))
                fileManager.removeItemAtURL(imageItem.fileurl!, error: nil)
            }
        }
        
        imageArray = imageArray.filter() { !$0.complete }
        
        collectionView?.reloadData()
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ItemCell", forIndexPath: indexPath) as! ImageCollectionViewCell
        
        var item = imageArray[indexPath.row]
        cell.completeIcon.hidden = !item.complete
        
        if let aImage = item.thumbnailImage {
            cell.imageView.image = aImage
        } else {
            item.targetCell = cell
        }
        
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {

        if indexPath.item < imageArray.count {
            var item = imageArray[indexPath.item]
            item.targetCell = nil
        }
    }
}


