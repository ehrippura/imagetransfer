//
//  ImageCollectionViewCell.swift
//  ImageTransfer
//
//  Created by Tzu-Yi Lin on 2014/8/4.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var completeIcon: UIImageView!

    override func prepareForReuse() {
        
        super.prepareForReuse()
        imageView.image = nil
        completeIcon.hidden = true
    }
}
